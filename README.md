# One PHP implementation of kata http://codekata.com/kata/kata13-counting-code-lines/

## Features

* State Pattern for handling the state of a single source code character with SourceReader providing the context
* Chain of Responsibility Pattern see TokenState::handleToken
* Singleton Pattern used in Token States and Line States

## Implementation Approach
* The goal of this kata was to train the state pattern on two levels
    * character state
    * line state
* The source code is read character by character
* The base loop is to read a character, determine the state and if the current line includes code
* One basic assumption is that the given source code is valid PHP code (see especially https://www.php.net/manual/en/language.basic-syntax.comments.php)
    * One simplification is not to handle shell-style comments and annotations - those are just source code

## HowTo

1. Clone this repo
1. Install the composer packages
1. run Symfony command `bin/console app:sloc <filepath>` providing a filepath

## Tests

The Tests for the Token States (which also test Line States) can be executed by running `vendor/bin/phpunit`.

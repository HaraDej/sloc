<?php

namespace App\Tests\Sloc;

use App\Sloc\SourceReader;
use PHPUnit\Framework\TestCase;

class SourceReaderTest extends TestCase
{
    public function testHardCase(): void
    {
        $source = ' /*****
 * This is a test program with 5 lines of code
 *  \/* no nesting allowed!
 //*****//***/// Slightly pathological comment ending...

public class Hello {
    public static final void main(String [] args) { // gotta love Java
        // Say hello
        System./*wait*/out./*for*/println/*it*/("Hello/*");
    }

}
';

        $this->executeTest($source, 5);
    }

    public function testStandardCase(): void
    {
        $source = '// This file contains 3 lines of code
public interface Dave {
    /**
     * count the number of lines in a file
     */
    int countLines(File inFile); // not the real signature!
}
';

        $this->executeTest($source, 3);
    }

    protected function executeTest(string $source, int $expectedLines)
    {
        $reader = new SourceReader();

        $reader->read($source);

        $this->assertEquals($expectedLines, $reader->lineCount());
    }
}

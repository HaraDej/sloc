<?php

namespace App\Tests\Sloc\State;

use App\Sloc\SourceReader;
use App\Sloc\State\Line\CountState;
use App\Sloc\State\Line\SkipState;
use App\Sloc\State\Token\LineBreakState;
use App\Sloc\State\Token\DoubleQuoteStringState;
use App\Sloc\State\Token\DoubleQuoteStringStartState;
use App\Sloc\State\Token\DoubleQuoteStringLineBreakState;
use App\Sloc\State\Token\DoubleQuoteStringEscapeState;
use App\Sloc\State\Token\DoubleQuoteStringEndState;
use PHPUnit\Framework\TestCase;

class DoubleQuoteStringStateTest extends TokenTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->reader->handleToken('"');
        $this->handleAndAssert('a', 0, DoubleQuoteStringState::class, CountState::class);
    }

    public function testNextIsDoubleQuoteStringState(): void
    {
        $this->handleAndAssert('a', 0, DoubleQuoteStringState::class, CountState::class);
    }

    public function testNextIsDoubleQuoteStringEndState(): void
    {
        $this->handleAndAssert('"', 0, DoubleQuoteStringEndState::class, CountState::class);
    }

    public function testNextIsDoubleQuoteStringLineBreakState(): void
    {
        $this->handleAndAssert(PHP_EOL, 1, DoubleQuoteStringLineBreakState::class, CountState::class);
    }

    public function testNextIsDoubleQuoteStringEscapeState(): void
    {
        $this->handleAndAssert('\\', 0, DoubleQuoteStringEscapeState::class, CountState::class);
    }

    public function testStringCounts(): void
    {
        $reader = new SourceReader();
        $reader->handleToken('"');
        $reader->handleToken('"');
        $reader->handleToken(PHP_EOL);
        $this->assertEquals(1, $reader->lineCount());
    }

    public function testMultilineString(): void
    {
        $reader = new SourceReader();

        $reader->handleToken('"');
        
        $this->assertInstanceOf(DoubleQuoteStringStartState::class, $reader->tokenState());
        $this->assertEquals(0, $reader->lineCount());
        $this->assertInstanceOf(CountState::class, $reader->lineState());

        $reader->handleToken(PHP_EOL);
        $this->assertInstanceOf(DoubleQuoteStringLineBreakState::class, $reader->tokenState());
        $this->assertEquals(1, $reader->lineCount());
        $this->assertInstanceOf(CountState::class, $reader->lineState());

        $reader->handleToken('"');
        $this->assertInstanceOf(DoubleQuoteStringEndState::class, $reader->tokenState());
        $this->assertEquals(1, $reader->lineCount());
        $this->assertInstanceOf(CountState::class, $reader->lineState());

        $reader->handleToken(PHP_EOL);
        $this->assertInstanceOf(LineBreakState::class, $reader->tokenState());
        $this->assertEquals(2, $reader->lineCount());
        $this->assertInstanceOf(SkipState::class, $reader->lineState());

        $reader->handleToken(PHP_EOL);
        $this->assertInstanceOf(LineBreakState::class, $reader->tokenState());
        $this->assertEquals(2, $reader->lineCount());
        $this->assertInstanceOf(SkipState::class, $reader->lineState());
    }
}

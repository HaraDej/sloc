<?php

namespace App\Tests\Sloc\State;

use App\Sloc\SourceReader;
use App\Sloc\State\Line\CountState;
use App\Sloc\State\Line\SkipState;
use App\Sloc\State\Token\LineBreakState;
use App\Sloc\State\Token\SingleQuoteStringState;
use App\Sloc\State\Token\SingleQuoteStringStartState;
use App\Sloc\State\Token\SingleQuoteStringLineBreakState;
use App\Sloc\State\Token\SingleQuoteStringEscapeState;
use App\Sloc\State\Token\SingleQuoteStringEndState;
use PHPUnit\Framework\TestCase;

class SingleQuoteStringEscapeStateTest extends TokenTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->reader->handleToken('\'');
        $this->handleAndAssert('\\', 0, SingleQuoteStringEscapeState::class, CountState::class);
    }

    public function testNextIsSingleQuoteStringState(): void
    {
        $this->handleAndAssert('a', 0, SingleQuoteStringState::class, CountState::class);
    }

    public function testQuoteNextIsSingleStringEndState(): void
    {
        $this->handleAndAssert('\'', 0, SingleQuoteStringState::class, CountState::class);
    }

    public function testNextIsSingleQuoteStringLineBreakState(): void
    {
        $this->handleAndAssert(PHP_EOL, 1, SingleQuoteStringLineBreakState::class, CountState::class);
    }

    public function testEscapeNextIsSingleQuoteStringState(): void
    {
        $this->handleAndAssert('\\', 0, SingleQuoteStringState::class, CountState::class);
    }
}

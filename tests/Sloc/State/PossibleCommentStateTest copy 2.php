<?php

namespace App\Tests\Sloc\State;

use App\Sloc\State\Line\CountState;
use App\Sloc\State\Line\SkipState;
use App\Sloc\State\Token\PossibleCommentState;
use App\Sloc\State\Token\SourceTokenState;
use App\Sloc\State\Token\BlockCommentStartState;
use App\Sloc\State\Token\LineCommentState;

class PossibleCommentStateTest extends TokenTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->handleAndAssert('/', 0, PossibleCommentState::class, SkipState::class);
    }

    public function testNextIsSourceTokenState(): void
    {
        $this->handleAndAssert('a', 0, SourceTokenState::class, CountState::class);
    }

    public function testNextIsLineCommentState(): void
    {
        $this->handleAndAssert('/', 0, LineCommentState::class, SkipState::class);
    }

    public function testNextIsBlockCommentStartState(): void
    {
        $this->handleAndAssert('*', 0, BlockCommentStartState::class, SkipState::class);
    }
}

<?php

namespace App\Tests\Sloc\State;

use App\Sloc\State\Line\SkipState;
use App\Sloc\State\Token\BlockCommentEndState;
use App\Sloc\State\Token\BlockCommentState;
use App\Sloc\State\Token\BlockCommentPossibleEndState;

class BlockCommentStateTest extends TokenTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->reader->handleToken('/');
        $this->reader->handleToken('*');
        $this->handleAndAssert('a', 0, BlockCommentState::class, SkipState::class);
    }

    public function testNextIsBlockCommentPossibleEndState(): void
    {
        $this->handleAndAssert('*', 0, BlockCommentPossibleEndState::class, SkipState::class);
    }

    public function testNextIsBlockCommentState(): void
    {
        $this->handleAndAssert('a', 0, BlockCommentState::class, SkipState::class);
    }
}

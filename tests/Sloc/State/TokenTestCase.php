<?php

namespace App\Tests\Sloc\State;

use App\Sloc\SourceReader;
use PHPUnit\Framework\TestCase;


class TokenTestCase extends TestCase
{
    protected SourceReader $reader;

    public function setUp(): void
    {
        $this->reader = new SourceReader();
    }

    protected function handleAndAssert(string $token, int $lineCountAfter, string $tokenStateAfter, string $lineStateAfter)
    {
        $this->reader->handleToken($token);
        $this->assertInstanceOf($tokenStateAfter, $this->reader->tokenState());
        $this->assertInstanceOf($lineStateAfter, $this->reader->lineState());
        $this->assertEquals($lineCountAfter, $this->reader->lineCount());
    }
}

<?php

namespace App\Tests\Sloc\State;

use App\Sloc\State\Line\CountState;
use App\Sloc\State\Line\SkipState;
use App\Sloc\State\Token\EofState;
use App\Sloc\State\Token\LineBreakState;
use App\Sloc\State\Token\PossibleCommentState;
use App\Sloc\State\Token\SourceTokenState;
use App\Sloc\State\Token\WhitespaceState;
use App\Sloc\State\Token\SingleQuoteStringStartState;
use App\Sloc\State\Token\DoubleQuoteStringStartState;

class EofStateTest extends TokenTestCase
{
    public function testState(): void
    {
        $this->reader->handleEof();

        $this->assertEquals(0, $this->reader->lineCount());
        $this->assertInstanceOf(SkipState::class, $this->reader->lineState());
        $this->assertInstanceOf(EofState::class, $this->reader->tokenState());
    }
}

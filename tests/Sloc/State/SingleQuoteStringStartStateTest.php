<?php

namespace App\Tests\Sloc\State;

use App\Sloc\SourceReader;
use App\Sloc\State\Line\CountState;
use App\Sloc\State\Line\SkipState;
use App\Sloc\State\Token\LineBreakState;
use App\Sloc\State\Token\SingleQuoteStringState;
use App\Sloc\State\Token\SingleQuoteStringStartState;
use App\Sloc\State\Token\SingleQuoteStringLineBreakState;
use App\Sloc\State\Token\SingleQuoteStringEscapeState;
use App\Sloc\State\Token\SingleQuoteStringEndState;
use PHPUnit\Framework\TestCase;

class SingleQuoteStringStartStateTest extends TokenTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->handleAndAssert('\'', 0, SingleQuoteStringStartState::class, CountState::class);
    }

    public function testNextIsSingleQuoteStringState(): void
    {
        $this->handleAndAssert('a', 0, SingleQuoteStringState::class, CountState::class);
    }

    public function testNextIsSingleQuoteStringEndState(): void
    {
        $this->handleAndAssert('\'', 0, SingleQuoteStringEndState::class, CountState::class);
    }

    public function testNextIsSingleQuoteStringLineBreakState(): void
    {
        $this->handleAndAssert(PHP_EOL, 1, SingleQuoteStringLineBreakState::class, CountState::class);
    }

    public function testNextIsSingleQuoteStringEscapeState(): void
    {
        $this->handleAndAssert('\\', 0, SingleQuoteStringEscapeState::class, CountState::class);
    }
}

<?php

namespace App\Tests\Sloc\State;

use App\Sloc\SourceReader;
use App\Sloc\State\Line\CountState;
use App\Sloc\State\Line\SkipState;
use App\Sloc\State\Token\LineBreakState;
use App\Sloc\State\Token\DoubleQuoteStringState;
use App\Sloc\State\Token\DoubleQuoteStringStartState;
use App\Sloc\State\Token\DoubleQuoteStringLineBreakState;
use App\Sloc\State\Token\DoubleQuoteStringEscapeState;
use App\Sloc\State\Token\DoubleQuoteStringEndState;
use PHPUnit\Framework\TestCase;

class DoubleQuoteStringLineBreakStateTest extends TokenTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->reader->handleToken('"');
        $this->handleAndAssert(PHP_EOL, 1, DoubleQuoteStringLineBreakState::class, CountState::class);
    }

    public function testNextIsDoubleQuoteStringState(): void
    {
        $this->handleAndAssert('a', 1, DoubleQuoteStringState::class, CountState::class);
    }

    public function testNextIsDoubleQuoteStringEndState(): void
    {
        $this->handleAndAssert('"', 1, DoubleQuoteStringEndState::class, CountState::class);
    }

    public function testNextIsDoubleQuoteStringLineBreakState(): void
    {
        $this->handleAndAssert(PHP_EOL, 2, DoubleQuoteStringLineBreakState::class, CountState::class);
    }

    public function testNextIsDoubleQuoteStringEscapeState(): void
    {
        $this->handleAndAssert('\\', 1, DoubleQuoteStringEscapeState::class, CountState::class);
    }
}

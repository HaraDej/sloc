<?php

namespace App\Tests\Sloc\State;

use App\Sloc\State\Line\CountState;
use App\Sloc\State\Line\SkipState;
use App\Sloc\State\Token\InitialState;
use App\Sloc\State\Token\LineBreakState;
use App\Sloc\State\Token\PossibleCommentState;
use App\Sloc\State\Token\SourceTokenState;
use App\Sloc\State\Token\WhitespaceState;
use App\Sloc\State\Token\SingleQuoteStringStartState;
use App\Sloc\State\Token\DoubleQuoteStringStartState;

class InitialStateTest extends TokenTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->assertEquals(0, $this->reader->lineCount());
        $this->assertInstanceOf(SkipState::class, $this->reader->lineState());
        $this->assertInstanceOf(InitialState::class, $this->reader->tokenState());
    }

    public function testNextIsLineBreakState(): void
    {
        $this->handleAndAssert(PHP_EOL, 0, LineBreakState::class, SkipState::class);
    }

    public function testNextIsPossibleCommentState(): void
    {
        $this->handleAndAssert('/', 0, PossibleCommentState::class, SkipState::class);
    }

    public function testNextIsSourceTokenState(): void
    {
        $this->handleAndAssert('a', 0, SourceTokenState::class, CountState::class);
    }

    public function testNextIsWhitespaceState(): void
    {
        $this->handleAndAssert(' ', 0, WhitespaceState::class, SkipState::class);
    }

    public function testNextIsSingleQuoteStringStartState(): void
    {
        $this->handleAndAssert('\'', 0, SingleQuoteStringStartState::class, CountState::class);
    }

    public function testNextIsDoubleQuoteStringStartState(): void
    {
        $this->handleAndAssert('"', 0, DoubleQuoteStringStartState::class, CountState::class);
    }
}

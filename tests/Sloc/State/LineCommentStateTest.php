<?php

namespace App\Tests\Sloc\State;

use App\Sloc\State\Line\SkipState;
use App\Sloc\State\Token\LineBreakState;
use App\Sloc\State\Token\LineCommentState;

class LineCommentStateTest extends TokenTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->reader->handleToken('/');
        $this->handleAndAssert('/', 0, LineCommentState::class, SkipState::class);
    }

    public function testNextIsLineCommentState(): void
    {
        $this->handleAndAssert('a', 0, LineCommentState::class, SkipState::class);
    }

    public function testNextIsLineBreakState(): void
    {
        $this->handleAndAssert(PHP_EOL, 0, LineBreakState::class, SkipState::class);
    }
}

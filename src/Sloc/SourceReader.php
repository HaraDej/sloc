<?php

namespace App\Sloc;

use App\Sloc\State\Line\LineState;
use App\Sloc\State\Token\TokenState;
use App\Sloc\State\Line\SkipState;
use App\Sloc\State\Token\InitialState;

use SplFileObject;

class SourceReader
{
    protected LineState $lineState;
    protected TokenState $tokenState;
    private $lineCount = 0;

    public function __construct()
    {
        $this->lineState = SkipState::instance();
        $this->tokenState = InitialState::instance();
    }

    public function readFile(string $filepath): void
    {
        $file = new SplFileObject($filepath, 'r');
        while (($token = $file->fgetc()) !== false) {
            $this->handleToken($token);
        }
        $this->handleEof();
    }

    public function read(string $source): void
    {
        while ($source !== '') {
            $token = mb_substr($source, 0, 1);
            $this->handleToken($token);
            $source = mb_substr($source, 1);
        }
        $this->handleEof();
    }

    public function handleToken(string $token): void
    {
        $this->tokenState->handleToken($token, $this);
    }

    public function handleEof(): void
    {
        $this->tokenState->handleEof($this);
    }

    public function changeTokenState(TokenState $tokenState): void
    {
        $this->tokenState = $tokenState;
    }

    public function changeLineState(LineState $lineState): void
    {
        $this->lineState = $lineState;
    }

    public function newLine(): void
    {
        $this->lineState->newLine($this);
    }

    public function setCountLineState(): void
    {
        $this->lineState->setCount($this);
    }

    public function incrementLineCount(): void
    {
        $this->lineCount++;
    }

    public function lineCount(): int
    {
        return $this->lineCount;
    }

    public function lineState()
    {
        return $this->lineState;
    }

    public function tokenState()
    {
        return $this->tokenState;
    }
}

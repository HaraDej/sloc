<?php

namespace App\Sloc\State\Token;

class PossibleCommentState extends TokenState
{
    protected function possibleNextStates(): array
    {
        return [
            LineCommentState::instance(),
            BlockCommentStartState::instance(),
        ];
    }

    protected function defaultNextState(): TokenState
    {
        return SourceTokenState::instance();
    }

    protected function accepts(string $token): bool
    {
        return $token === '/';
    }
}

<?php

namespace App\Sloc\State\Token;

use App\Sloc\SourceReader;

class SingleQuoteStringState extends TokenState
{
    protected function possibleNextStates(): array
    {
        return [
            SingleQuoteStringEndState::instance(),
            SingleQuoteStringEscapeState::instance(),
            SingleQuoteStringLineBreakState::instance(),
        ];
    }

    protected function defaultNextState(): TokenState
    {
        return SingleQuoteStringState::instance();
    }

    protected function accepts(string $token): bool
    {
        return true;
    }
}

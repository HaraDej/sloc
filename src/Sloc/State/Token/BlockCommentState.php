<?php

namespace App\Sloc\State\Token;

class BlockCommentState extends TokenState
{
    protected function possibleNextStates(): array
    {
        return [
            BlockCommentPossibleEndState::instance(),
        ];
    }

    protected function defaultNextState(): TokenState
    {
        return self::instance();
    }

    protected function accepts(string $token): bool
    {
        return true;
    }
}

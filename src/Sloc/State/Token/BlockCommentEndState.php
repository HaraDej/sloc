<?php

namespace App\Sloc\State\Token;

class BlockCommentEndState extends TokenState
{
    protected function defaultNextState(): TokenState
    {
        return SourceTokenState::instance();
    }

    protected function accepts(string $token): bool
    {
        return $token === '/';
    }
}

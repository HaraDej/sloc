<?php

namespace App\Sloc\State\Token;

class BlockCommentStartState extends TokenState
{
    protected function possibleNextStates(): array
    {
        return [
            BlockCommentPossibleEndState::instance(),
        ];
    }

    protected function defaultNextState(): TokenState
    {
        return BlockCommentState::instance();
    }

    protected function accepts(string $token): bool
    {
        return $token === '*';
    }
}

<?php

namespace App\Sloc\State\Token;

use App\Sloc\SourceReader;

class DoubleQuoteStringLineBreakState extends TokenState
{
    protected function possibleNextStates(): array
    {
        return [
            DoubleQuoteStringEndState::instance(),
            DoubleQuoteStringEscapeState::instance(),
            DoubleQuoteStringLineBreakState::instance(),
        ];
    }

    protected function defaultNextState(): TokenState
    {
        return DoubleQuoteStringState::instance();
    }

    protected function accepts(string $token): bool
    {
        return $token === PHP_EOL;
    }

    protected function tokenAction(SourceReader $reader): void
    {
        $reader->newLine();
        $reader->setCountLineState();
    }
}

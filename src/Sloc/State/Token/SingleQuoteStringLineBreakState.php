<?php

namespace App\Sloc\State\Token;

use App\Sloc\SourceReader;

class SingleQuoteStringLineBreakState extends TokenState
{
    protected function possibleNextStates(): array
    {
        return [
            SingleQuoteStringEndState::instance(),
            SingleQuoteStringEscapeState::instance(),
            SingleQuoteStringLineBreakState::instance(),
        ];
    }

    protected function defaultNextState(): TokenState
    {
        return SingleQuoteStringState::instance();
    }

    protected function accepts(string $token): bool
    {
        return $token === PHP_EOL;
    }

    protected function tokenAction(SourceReader $reader): void
    {
        $reader->newLine();
        $reader->setCountLineState();
    }
}

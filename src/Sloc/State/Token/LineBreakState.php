<?php

namespace App\Sloc\State\Token;

use App\Sloc\SourceReader;
use App\Sloc\State\Line\CountState;

class LineBreakState extends TokenState
{
    protected function defaultNextState(): TokenState
    {
        return SourceTokenState::instance();
    }

    protected function accepts(string $token): bool
    {
        return $token === PHP_EOL;
    }

    protected function tokenAction(SourceReader $reader): void
    {
        $reader->newLine();
    }
}

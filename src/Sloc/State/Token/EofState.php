<?php

namespace App\Sloc\State\Token;

use App\Sloc\SourceReader;

class EofState extends TokenState
{
    protected function possibleNextStates(): array
    {
        return [];
    }

    protected function defaultNextState(): TokenState
    {
        return self::instance();
    }

    protected function accepts(string $token): bool
    {
        return true;
    }

    protected function tokenAction(SourceReader $reader): void
    {
        $reader->newLine();
    }
}

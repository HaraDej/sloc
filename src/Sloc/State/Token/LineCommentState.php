<?php

namespace App\Sloc\State\Token;

class LineCommentState extends TokenState
{
    protected function possibleNextStates(): array
    {
        return [
            LineBreakState::instance(),
        ];
    }

    protected function defaultNextState(): TokenState
    {
        return self::instance();
    }

    protected function accepts(string $token): bool
    {
        return $token === '/';
    }
}

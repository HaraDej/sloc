<?php

namespace App\Sloc\State\Token;

class DoubleQuoteStringEscapeState extends TokenState
{
    protected function possibleNextStates(): array
    {
        return [
        ];
    }

    protected function defaultNextState(): TokenState
    {
        return DoubleQuoteStringState::instance();
    }

    protected function accepts(string $token): bool
    {
        return $token === '\\';
    }
}

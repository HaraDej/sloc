<?php

namespace App\Sloc\State\Token;

use App\Sloc\SourceReader;

abstract class TokenState
{
    protected static $instance;

    protected function possibleNextStates(): array
    {
        return [
            LineBreakState::instance(),
            PossibleCommentState::instance(),
            WhitespaceState::instance(),
            SingleQuoteStringStartState::instance(),
            DoubleQuoteStringStartState::instance(),
        ];
    }

    abstract protected function defaultNextState(): TokenState;

    abstract protected function accepts(string $token): bool;

    public static function instance(): static
    {
        if (! static::$instance instanceof static) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    protected function tokenAction(SourceReader $reader): void
    {
    }

    public function handleToken(string $token, SourceReader $reader): void
    {
        foreach ($this->possibleNextStates() as $possible) {
            if ($possible->accepts($token)) {
                $possible->tokenAction($reader);
                $reader->changeTokenState($possible);
                return;
            }
        }

        $default = $this->defaultNextState();
        $default->tokenAction($reader);
        $reader->changeTokenState($default);
    }

    public function handleEof(SourceReader $reader): void
    {
        $eofState = EofState::instance();
        $eofState->tokenAction($reader);
        $reader->changeTokenState($eofState);
    }
}

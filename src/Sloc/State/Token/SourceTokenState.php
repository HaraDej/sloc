<?php

namespace App\Sloc\State\Token;

use App\Sloc\SourceReader;

class SourceTokenState extends TokenState
{
    protected function defaultNextState(): TokenState
    {
        return self::instance();
    }

    protected function accepts(string $token): bool
    {
        return true;
    }

    protected function tokenAction(SourceReader $reader): void
    {
        $reader->setCountLineState();
    }
}

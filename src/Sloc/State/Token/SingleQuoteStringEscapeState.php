<?php

namespace App\Sloc\State\Token;

class SingleQuoteStringEscapeState extends TokenState
{
    protected function possibleNextStates(): array
    {
        return [
            SingleQuoteStringLineBreakState::instance(),
        ];
    }

    protected function defaultNextState(): TokenState
    {
        return SingleQuoteStringState::instance();
    }

    protected function accepts(string $token): bool
    {
        return $token === '\\';
    }
}

<?php

namespace App\Sloc\State\Token;

class BlockCommentPossibleEndState extends TokenState
{
    protected function possibleNextStates(): array
    {
        return [
            BlockCommentPossibleEndState::instance(),
            BlockCommentEndState::instance(),
        ];
    }

    protected function defaultNextState(): TokenState
    {
        return BlockCommentState::instance();
    }

    protected function accepts(string $token): bool
    {
        return $token === '*';
    }
}

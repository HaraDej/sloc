<?php

namespace App\Sloc\State\Line;

use App\Sloc\SourceReader;

class LineState
{
    protected static $instance;

    public static function instance(): static
    {
        if (! static::$instance instanceof static) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function newLine(SourceReader $reader): void
    {
        $reader->changeLineState(SkipState::instance());
    }

    public function setCount(SourceReader $reader): void
    {
        $reader->changeLineState(CountState::instance());
    }
}

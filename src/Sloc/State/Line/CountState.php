<?php

namespace App\Sloc\State\Line;

use App\Sloc\SourceReader;

class CountState extends LineState
{
    public function newLine(SourceReader $reader): void
    {
        $reader->incrementLineCount();
        parent::newLine($reader);
    }
}

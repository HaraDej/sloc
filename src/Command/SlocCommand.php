<?php

namespace App\Command;

use App\Sloc\SourceReader;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

// the name of the command is what users type after "php bin/console"
#[AsCommand(name: 'app:sloc')]
class SlocCommand extends Command
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $reader = new SourceReader();

        $filepath = $input->getArgument('filepath');

        $reader->readFile($filepath);

        $output->writeln('File ' . $filepath . ' contains ' . $reader->lineCount() . ' source lines of code.');

        return Command::SUCCESS;
    }

    protected function configure(): void
    {
        $this->addArgument('filepath', InputArgument::REQUIRED, 'filepath for calculating SLOC');
    }
}
